<?php

class pluginTemplateToPostApi extends Plugin
{

    public function init()
    {
        $this->dbFields = array(
            'template' => '',
            'publish_time' => 'wednesday 12:00',
            'apiToken' => 'PlEaSeChAnGeThIsToSoMeRaNdOmStRiNg'
        );
    }


    public function form()
    {
        global $L, $staticPages;
        $html = '<style type="text/css">
            .plugin-form label {margin-top: 0 !important; }
            </style>';

        $html .= Bootstrap::formTitle(array('title' => $L->get('General')));
        $options = array('' => '');
        try {
            $selectedPage = $this->getValue('template');
            if ($selectedPage) {
                $page = new Page($selectedPage);
                $options[$selectedPage] = $page->title();
            }
        } catch (Exception $e) {
            // noop
        }

        $html .= Bootstrap::formSelect(array(
            'name' => 'template',
            'label' => 'Template',
            'options' => $options,
            'selected' => $this->getValue('template')
        ));

        $html .= '<script>
    $(document).ready(function() {
      var pageSelect = $("#jstemplate");
      pageSelect.select2({
        allowClear: true,
        theme: "bootstrap4",
        minimumInputLength: 2,
        ajax: {
          url: "/get-drafts",
          data: function (params) {
            var query = { query: params.term }
            return query;
          },
          processResults: function (data) {
            return data;
          }
        },
        escapeMarkup: function(markup) {
          return markup;
        }
      })
    });
    </script>';

        // email receiver
        $html .= Bootstrap::formInputText(array(
            'name' => 'publish_time',
            'label' => 'Zeitpunkt Veröffentlichung',
            'value' => $this->getValue('publish_time'),
            'tip' => "Syntax/Beispiele: <a href=\"https://www.php.net/manual/de/datetime.formats.relative.php\">https://www.php.net/manual/de/datetime.formats.relative.php</a><br>\"".$this->getValue('publish_time')."\" wird aktuell zu " . date('Y-m-d H:i:s', strtotime($this->getValue('publish_time')))
        ));

        // email receiver name
        $html .= Bootstrap::formInputText(array(
            'name' => 'apiToken',
            'label' => $L->get('Api Token'),
            'value' => $this->getValue('apiToken'),
        ));


        return $html;
    }

    // send email and redirect to page with contact form
    public function beforeAll()
    {
        if($this->webhook('get-drafts')){
            $this->getDrafts();
        }

        if ($this->webhook('templateToPostApi')) {
            $this->api();
        }
        return;
    }


    private function getDrafts(){
        global $pages;
        header('Content-Type: application/json');

        $query = isset($_GET['query']) ? Text::lowercase($_GET['query']) : false;

        $checkIsParent = empty($_GET['checkIsParent']) ? false : true;
        if ($query===false) {
            ajaxResponse(1, 'Invalid query.');
        }
        $result = array();
        $pagesKey = $pages->getDB();
        foreach ($pagesKey as $pageKey) {
            try {
                $page = new Page($pageKey);
                if ($page->isParent() || !$checkIsParent) {
                    if ($page->draft()) {
                        $lowerTitle = Text::lowercase($page->title());
                        if (Text::stringContains($lowerTitle, $query)) {
                            $tmp = array('disabled'=>false);
                            $tmp['id'] = $page->key();
                            $tmp['text'] = $page->title();
                            $tmp['type'] = $page->type();
                            array_push($result, $tmp);
                        }
                    }
                }
            } catch (Exception $e) {
                // noop
            }
        }

        exit (json_encode(array('results'=>$result)));
    }

    private function api(){
        header('Content-Type: application/json');
        $input = json_decode(file_get_contents('php://input'));
        if(!$input instanceof \stdClass){
            $this->response(400,"Invalid JSON");
        }
        if(!isset($input->apiToken) || $input->apiToken !== $this->getValue('apiToken')){
            $this->response(401,"Unauthorized");
        }
        $this->createPost($input);
        exit(0);
    }


    private function response($code, $message){
        http_response_code($code);
        exit(json_encode(array('code'=>$code,'message' => $message)));
    }

    public function createPost($data){
        global $url;
        global $pages;

        $template = buildPage($this->getValue('template'));
        if(!$template instanceof Page){
            $this->response(500,"Template not found");
        }

        $timestamp = strtotime($this->getValue('publish_time'));
        $data->germanDate = $this->toGermanDay((int)date('N',$timestamp)).', '.date('d',$timestamp).'. '.$this->intToGermanMonth((int)date('m',$timestamp)).' '.date('Y',$timestamp);

        $page = array();
        $page['title'] = $this->replaceVars($template->title(), $data);
        $page['slug'] = $this->replaceVars($template->slug(), $data);
        $page['content'] = $this->replaceVars($template->content(),$data);
        $page['type'] = 'draft';
        if(isset($data->publish) && filter_var($data->publish, FILTER_VALIDATE_BOOLEAN)){
            $page['type'] = 'published';
        }
        $page['date'] = date('Y-m-d H:i:s',$timestamp);
        $page['tags'] = $this->replaceVars($template->tags(false),$data);
        $page['description'] = $this->replaceVars($template->description(),$data);
        $page['category'] = $template->categoryKey();
        $page['username'] = $template->username();
        $key = $pages->add($page);
        if ($key) {
            // Call the plugins after page created
            Theme::plugins('afterPageCreate', array($key));

            reindexCategories();
            reindexTags();
        }
        $this->response(200,"Artikel erfolgreich erstellt");
    }

    private function replaceVars($content,$data){
        foreach($data as $key => $value){
            if(is_string($value)){
                $content = str_replace('°'.$key.'°',$value,$content);
            }
        }
        return $content;
    }

    private function replaceArrayWithVars($array,$data){
        $result = array();
        foreach($array as $value){
            $result[] = $this->replaceVars($value,$data);
        }
        return $result;
    }

    private function getBaseUrl()
    {
        $protocol = 'http';
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $protocol = 'https';
        }
        $host = $_SERVER['HTTP_HOST'];
        return $protocol . '://' . $host;
    }

    private function intToGermanMonth($month)
    {
        switch ($month) {
            case 1:
                return 'Januar';
            case 2:
                return 'Februar';
            case 3:
                return 'März';
            case 4:
                return 'April';
            case 5:
                return 'Mai';
            case 6:
                return 'Juni';
            case 7:
                return 'Juli';
            case 8:
                return 'August';
            case 9:
                return 'September';
            case 10:
                return 'Oktober';
            case 11:
                return 'November';
            case 12:
                return 'Dezember';
        }
    }


    private function toGermanDay($day)
    {
        switch ($day) {
            case 1:
                return 'Mo';
            case 2:
                return 'Di';
            case 3:
                return 'Mi';
            case 4:
                return 'Do';
            case 5:
                return 'Fr';
            case 6:
                return 'Sa';
            case 7:
                return 'So';
        }
    }

}
